const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const UID = require('uid');
const assign = require('./lib/polyfill/assign.js');
const log = require('./lib/log.js').default;
const port = process.env.PORT || process.argv[2] || 80

var users = [];

function newUser(uid) {return {uid: uid||UID(), name: 'Anonymous'};}

function userIndex(uid) {
  for (var i = users.length - 1; i >= 0; i--) {
    if (users[i].uid != uid) continue;
    return i;
  }
}

app.use(express.static('public'))

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/views/index.html')
});

io.sockets.on('connection', function (socket) {

  var uid = UID(),
      user = function(){return users[userIndex(uid)]}

  socket.emit('setup', {user:  newUser(uid), users: users}, function(data) {
    users.push(newUser(uid))
    
    log(user().name+'('+user().uid+') has joined', 'info')

    socket.broadcast.emit('user-new', {user: user(), count: users.length})
  });

  socket.on('users', function(d, callback) {
    log(user().name+'('+uid+') lost the online user count', 'warn')
    callback({users: users})
  });

  socket.on('user-change', function(data) {
    var oldUser = user()
    user().name = data.user.name
    log( oldUser.name + '('+uid+') changed to ' + user().name+'('+uid+')', 'info')
    socket.broadcast.emit('user-change', {from: oldUser, to: user()})
  });

  socket.on('disconnect', function() {
    var disconnected = users.splice(userIndex(uid), 1)[0]
    log(disconnected.name+'('+uid+')'+' is gone', 'info')
    socket.broadcast.emit('user-out', {user: disconnected, count:users.length})
  });

  socket.on('msg', function (data) {
    io.sockets.emit('msg', {msg: data.msg, uid: uid})
  });
});


server.listen(port)

log('Server started at port :'+ port)

