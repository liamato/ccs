var socket = io();

var list = document.getElementById('msg-list');
var input = document.getElementById('send-msg');
var btn = document.getElementById('send-btn');
var bottom = document.getElementById('bottom');
var ucount = document.getElementById('users-count');
var uname = document.getElementById('user-name');

var user, users = [];

function userIndex(uid) {
  for (var i = users.length - 1; i >= 0; i--) {
    if (users[i].uid == uid) {
      return i;
      break;
    }
  }
}

var template = function(temp) {
  return document.getElementById('templates').getElementsByClassName(temp)[0].cloneNode(true);
};

var templateFindChild = function(className, node) {
  return node.getElementsByClassName(className)[0];
};

var changeUserNamesByUid = function(userName, uid) {
  var nameList = document.getElementById('msg-list').querySelectorAll('[data-uid="'+uid+'"]');
  nameList.forEach(function(msg) {
    msg.replaceChild(document.createTextNode(userName), msg.childNodes[0]);
  });
}

var nodedLinketize = function(string) {

  var re = /https?:\/\/[0-9a-zA-Z\$\-_\.\+\!\*\'\(\)\,\;\/\?\:\@\=\&]+/gim

  if (!string.match(re)) return [document.createTextNode(string)]

  var res = []
  var match, lastIndex = 0

  var linketize = function(string) {
    var a = document.createElement('a')
    a.setAttribute('href', string)
    a.appendChild(document.createTextNode(string))
    return a
  }

  while ((match = re.exec(string)) !== null) {
    res.push(document.createTextNode(string.slice(lastIndex, match.index)))
    res.push(linketize(match[0]))
    lastIndex = re.lastIndex
  }

  if (re.lastIndex != string.length) res.push(document.createTextNode(string.slice(lastIndex, string.length)))

  return res
}

var decodeHtmlEntities = function(str) {
  return str.replace(/&#?(\w+);/g, function(match, dec) {
    if(isNaN(dec)) {
      chars = {quot: 34, amp: 38, lt: 60, gt: 62, nbsp: 160, copy: 169, reg: 174, deg: 176, frasl: 47, trade: 8482, euro: 8364, Agrave: 192, Aacute: 193, Acirc: 194, Atilde: 195, Auml: 196, Aring: 197, AElig: 198, Ccedil: 199, Egrave: 200, Eacute: 201, Ecirc: 202, Euml: 203, Igrave: 204, Iacute: 205, Icirc: 206, Iuml: 207, ETH: 208, Ntilde: 209, Ograve: 210, Oacute: 211, Ocirc: 212, Otilde: 213, Ouml: 214, times: 215, Oslash: 216, Ugrave: 217, Uacute: 218, Ucirc: 219, Uuml: 220, Yacute: 221, THORN: 222, szlig: 223, agrave: 224, aacute: 225, acirc: 226, atilde: 227, auml: 228, aring: 229, aelig: 230, ccedil: 231, egrave: 232, eacute: 233, ecirc: 234, euml: 235, igrave: 236, iacute: 237, icirc: 238, iuml: 239, eth: 240, ntilde: 241, ograve: 242, oacute: 243, ocirc: 244, otilde: 245, ouml: 246, divide: 247, oslash: 248, ugrave: 249, uacute: 250, ucirc: 251, uuml: 252, yacute: 253, thorn: 254, yuml: 255, lsquo: 8216, rsquo: 8217, sbquo: 8218, ldquo: 8220, rdquo: 8221, bdquo: 8222, dagger: 8224, Dagger: 8225, permil: 8240, lsaquo: 8249, rsaquo: 8250, spades: 9824, clubs: 9827, hearts: 9829, diams: 9830, oline: 8254, larr: 8592, uarr: 8593, rarr: 8594, darr: 8595, hellip: 133, ndash: 150, mdash: 151, iexcl: 161, cent: 162, pound: 163, curren: 164, yen: 165, brvbar: 166, brkbar: 166, sect: 167, uml: 168, die: 168, ordf: 170, laquo: 171, not: 172, shy: 173, macr: 175, hibar: 175, plusmn: 177, sup2: 178, sup3: 179, acute: 180, micro: 181, para: 182, middot: 183, cedil: 184, sup1: 185, ordm: 186, raquo: 187, frac14: 188, frac12: 189, frac34: 190, iquest: 191, Alpha: 913, alpha: 945, Beta: 914, beta: 946, Gamma: 915, gamma: 947, Delta: 916, delta: 948, Epsilon: 917, epsilon: 949, Zeta: 918, zeta: 950, Eta: 919, eta: 951, Theta: 920, theta: 952, Iota: 921, iota: 953, Kappa: 922, kappa: 954, Lambda: 923, lambda: 955, Mu: 924, mu: 956, Nu: 925, nu: 957, Xi: 926, xi: 958, Omicron: 927, omicron: 959, Pi: 928, pi: 960, Rho: 929, rho: 961, Sigma: 931, sigma: 963, Tau: 932, tau: 964, Upsilon: 933, upsilon: 965, Phi: 934, phi: 966, Chi: 935, chi: 967, Psi: 936, psi: 968, Omega: 937, omega: 969}
      if (chars[dec] !== undefined){
        dec = chars[dec];
      }
    }
    return String.fromCharCode(dec);
  });
};

var encodeHtmlEntity = function(str) {
  var buf = [];
  for (var i=str.length-1;i>=0;i--) {
    buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
  }
  return buf.join('');
};

socket.on('connect',function(){

  function updateCount(count) {
    ucount.textContent = users.length

    if (users.length == (count||users.length)) return;

    socket.emit('users','',function(data){
      users = data.users
      updateCount()
    });
  }

  socket.on('setup', function(data, ack){
    user = data.user;
    uname.value = user.name;
    users = data.users;
    users.push(user)
    updateCount()
    ack();
  })

  var saveUser = function() {
    var tmp = user.name;
    user.name = uname.value || 'Anonymous';
    if (user.name !== tmp) socket.emit('user-change', {user: user});
  };

  input.addEventListener('keypress', function(ev){
    if (!ev.ctrlKey && ev.keyCode == 13) {
      ev.preventDefault();
      if (input.value) {
        socket.emit('msg', {msg: encodeHtmlEntity(input.value)});
        input.value = "";
      }
    }

    if (ev.ctrlKey && ev.keyCode == 10) {
      input.value += '\n'
      input.scrollTop = input.scrollHeight
    }
  });

  btn.addEventListener('click', function(ev){
    ev.preventDefault();
    if (input.value) {
      socket.emit('msg', {msg: encodeHtmlEntity(input.value)});
      input.value = "";
    }
  });

  document.getElementById('user-save').addEventListener('click', function(ev) {
    ev.preventDefault();
    saveUser();
  });



  socket.on('msg', function(data){
    var temp = template('msg');
    var text = templateFindChild('msg__text',temp)
    var textNodes = nodedLinketize(decodeHtmlEntities(data.msg))
    for (var i = 0; i < textNodes.length; i++) {
      text.appendChild(textNodes[i])
    }
    var usr = templateFindChild('msg__user',temp)
    usr.appendChild(document.createTextNode(decodeHtmlEntities((data.uid == user.uid ? 'Me' : users[userIndex(data.uid)].name))));
    usr.setAttribute('data-uid',data.uid);
    list.appendChild(temp);
    bottom.scrollIntoView();
  });

  socket.on('user-change', function(data){
    changeUserNamesByUid(data.to.name, data.to.uid);

    var li = document.createElement('li');
    li.appendChild(document.createTextNode(decodeHtmlEntities(data.from.name + ' changed to ' + data.to.name)));
    list.appendChild(li);
    bottom.scrollIntoView();
  });

  socket.on('user-new', function(data) {
    var li = document.createElement('li');
    li.appendChild(document.createTextNode(decodeHtmlEntities(data.user.name + ' has joined')));
    list.appendChild(li);
    bottom.scrollIntoView();

    users.push(data.user)

    updateCount(data.count)
  });

  socket.on('user-out', function(data) {
    var li = document.createElement('li');
    li.appendChild(document.createTextNode(decodeHtmlEntities(data.user.name + ' is gone')));
    list.appendChild(li);
    bottom.scrollIntoView();

    users.splice(userIndex(data.user.uid), 1);

    updateCount(data.count)
  });

});

