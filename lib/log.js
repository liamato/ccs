function log(mesage, lvl, obj) {
  if (!lvl) lvl = 'log';
  var d = new Date(Date.now());
  var msg = '['+pad(d.getDate(), 2)+'-'+pad(d.getMonth()+1, 2)+'-'+d.getFullYear()+' '+pad(d.getHours(), 2)+':'+pad(d.getMinutes(), 2)+':'+pad(d.getSeconds(), 2)+'] '+(UCFirst(lvl)||'Info')+': '+mesage;
  if (obj) msg += '\n\t'+JSON.stringify(obj)+'\n'

  if (!console[lvl]) {
    console.log(msg)
    return
  }

  console[lvl](msg)
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function UCFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = {default: log, pad: pad}
